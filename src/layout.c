/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/layout.c
 * Layout object handling
 */

#include <assert.h>
#include <stdlib.h>

#include "libnslayout/nslayout.h"

#include "layout.h"
#include "util/util.h"
#include "util/dom-str.h"


/**
 * The layout object for a DOM document
 */
struct nsl_layout {
	dom_document *document;
	css_select_ctx *css_ctx;
	css_media_type *media;
	nsl_callback cb;
	void *pw;
};


/* Publically exported function, documented in include/libnslayout/nslayout.h */
nsl_error nsl_init(void)
{
	return nsl_dom_str_init();
}


/* Publically exported function, documented in include/libnslayout/nslayout.h */
nsl_error nsl_fini(void)
{
	return nsl_dom_str_fini();
}


/* Publically exported function, documented in include/libnslayout/nslayout.h */
nsl_error nsl_layout_create(
		dom_document *doc,
		css_select_ctx *css_ctx,
		css_media_type *media,
		nsl_callback cb,
		void *pw,
		nsl_layout **layout)
{
	nsl_layout *l = NULL;

	assert(doc != NULL);
	assert(css_ctx != NULL);
	assert(media != NULL);
	assert(cb != NULL);

	l = calloc(1, sizeof(nsl_layout));
	if (l == NULL) {
		return NSL_NO_MEM;
	}

	/* TODO: Decide: ownership will probably be passed to libnslayout */
	l->document = doc;
	l->css_ctx = css_ctx;
	l->media = media;
	l->cb = cb;
	l->pw = pw;

	*layout = l;
	return NSL_OK;
}


/* Publically exported function, documented in include/libnslayout/nslayout.h */
nsl_error nsl_layout_destroy(
		nsl_layout *layout)
{
	assert(layout != NULL);

	/* TODO: free/unref the stuff we own in the layout */

	free(layout);
	return NSL_OK;
}

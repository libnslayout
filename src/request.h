/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/request.h
 * Client callback wrappers
 */

#ifndef nsl_request_h_
#define nsl_request_h_

#include <libnslayout/nslayout.h>

/**
 * Perform GET_RESOURCE client callback
 *
 * \param[in]  layout    Layout object that the request concerns.
 * \param[in]  url       Absolute URL to request replaced object handle for.
 * \param[out] replaced  Returns the replaced element content handle.
 * \return NSL_OK on success, appropriate error otherwise.
 */
static inline nsl_error nsl_request_get_resource(
		nsl_layout *layout,
		const char *url,
		nsl_replaced **replaced)
{
	nsl_error err;
	nsl_request req;

	req.type = NSL_GET_RESOURCE;
	req.request.get_resource.url = url;

	err = layout->cb(layout, layout->pw, &req);

	*replaced = *req.response.get_resource.replaced;
	return err;
}

/**
 * Perform CREATE_REPLACED client callback
 *
 * \param[in]  layout    Layout object that the request concerns.
 * \param[in]  element   DOM element that needs replacement object.
 * \param[out] replaced  Returns the replaced element content handle.
 * \return NSL_OK on success, appropriate error otherwise.
 */
static inline nsl_error nsl_request_create_replaced(
		nsl_layout *layout,
		dom_element *element,
		nsl_replaced **replaced)
{
	nsl_error err;
	nsl_request req;

	req.type = NSL_CREATE_REPLACED;
	req.request.create_replaced.element = element;

	err = layout->cb(layout, layout->pw, &req);

	*replaced = *req.response.create_replaced.replaced;
	return err;
}

/**
 * Perform RENDER client callback
 *
 * \param[in] layout  Layout object being rendered.
 * \param[in] list    Render list to render.
 * \return NSL_OK on success, appropriate error otherwise.
 */
static inline nsl_error nsl_request_render(
		nsl_layout *layout,
		nsl_render_list *list)
{
	nsl_error err;
	nsl_request req;

	req.type = NSL_RENDER;
	req.request.render.list = list;

	err = layout->cb(layout, layout->pw, &req);

	return err;
}

/**
 * Perform SET_EXTENTS client callback
 *
 * \param[in] layout  Layout object that the request concerns.
 * \param[in] width   The layout's full width.
 * \param[in] height  The layout's full height.
 * \return NSL_OK on success, appropriate error otherwise.
 */
static inline nsl_error nsl_request_set_extents(
		nsl_layout *layout,
		unsigned int width,
		unsigned int height)
{
	nsl_error err;
	nsl_request req;

	req.type = NSL_SET_EXTENTS;
	req.request.set_extents.width  = width;
	req.request.set_extents.height = height;

	err = layout->cb(layout, layout->pw, &req);

	return err;
}

/**
 * Perform GET_INTRINSIC_SIZE client callback
 *
 * \param[in]  layout    Layout object that the request concerns.
 * \param[in]  replaced  Replaced object to get intrinsic size of.
 * \param[out] width     Returns the replaced object's width.
 * \param[out] height    Returns the replaced object's height.
 * \return NSL_OK on success, appropriate error otherwise.
 */
static inline nsl_error nsl_request_get_intrinsic_size(
		nsl_layout *layout,
		nsl_replaced *replaced,
		unsigned int *width,
		unsigned int *height)
{
	nsl_error err;
	nsl_request req;

	req.type = NSL_GET_INTRINSIC_SIZE;
	req.request.get_intrinsic_size.replaced = replaced;

	err = layout->cb(layout, layout->pw, &req);

	*width  = *req.response.get_intrinsic_size.width;
	*height = *req.response.get_intrinsic_size.height;
	return err;
}

#endif

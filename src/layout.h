/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/layout.h
 * Layout object handling
 */

#ifndef nsl_layout_h_
#define nsl_layout_h_

struct nsl_layout;

#endif

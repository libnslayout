/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/util/dom-str.h
 * Layout object handling
 */

#ifndef nsl_util_dom_str_h_
#define nsl_util_dom_str_h_

extern dom_string *nsl_dom_str_node_inserted;
extern dom_string *nsl_dom_str_node_removed;
extern dom_string *nsl_dom_str_subtree_modified;
extern dom_string *nsl_dom_str_attr_modified;
extern dom_string *nsl_dom_str_characterdata_modified;

/**
 * Create the internal DOM strings
 *
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_dom_str_init(void);

/**
 * Unref the internal DOM strings
 *
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_dom_str_fini(void);

#endif

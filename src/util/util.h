/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/util/util.h
 * Layout object handling
 */

#ifndef nsl_util_util_h_
#define nsl_util_util_h_

#ifndef UNUSED
#define UNUSED(x_) (void)(x_)
#endif

#ifndef SLEN
#define SLEN(x_) (sizeof((x_)) - 1)
#endif

#define NSL_DOM_ERR(x_) ((x_ << 8) | NSL_LIBDOM)
#define NSL_CSS_ERR(x_) ((x_ << 8) | NSL_LIBCSS)

#endif

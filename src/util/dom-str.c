/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/util/dom-str.c
 * Layout object handling
 */

#include <stdio.h>

#include "libnslayout/nslayout.h"

#include "util/dom-str.h"
#include "util/util.h"

dom_string *nsl_dom_str_node_inserted;
dom_string *nsl_dom_str_node_removed;
dom_string *nsl_dom_str_subtree_modified;
dom_string *nsl_dom_str_attr_modified;
dom_string *nsl_dom_str_characterdata_modified;


/* Exported function, documented in src/util/dom-str.h */
nsl_error nsl_dom_str_init(void)
{
	nsl_error err;
	dom_exception exc;

	exc = dom_string_create((const uint8_t *)"DOMNodeInserted",
			SLEN("DOMNodeInserted"),
			&nsl_dom_str_node_inserted);
	if (exc != DOM_NO_ERR) {
		goto out;
	}
	exc = dom_string_create((const uint8_t *)"DOMNodeRemoved",
			SLEN("DOMNodeRemoved"),
			&nsl_dom_str_node_removed);
	if (exc != DOM_NO_ERR) {
		goto out;
	}
	exc = dom_string_create((const uint8_t *)"DOMSubtreeModified",
			SLEN("DOMSubtreeModified"),
			&nsl_dom_str_subtree_modified);
	if (exc != DOM_NO_ERR) {
		goto out;
	}
	exc = dom_string_create((const uint8_t *)"DOMAttrModified",
			SLEN("DOMAttrModified"),
			&nsl_dom_str_attr_modified);
	if (exc != DOM_NO_ERR) {
		goto out;
	}
	exc = dom_string_create((const uint8_t *)"DOMCharacterDataModified",
			SLEN("DOMCharacterDataModified"),
			&nsl_dom_str_characterdata_modified);
	if (exc != DOM_NO_ERR) {
		goto out;
	}

	err = NSL_OK;
out:
	if (exc != DOM_NO_ERR) {
		printf("Failed to initialise dom_str!\n");
		nsl_dom_str_fini();
		err = NSL_DOM_ERR(exc);
	}

	return err;
}


/* Exported function, documented in src/util/dom-str.h */
nsl_error nsl_dom_str_fini(void)
{
	dom_string_unref(nsl_dom_str_node_inserted);
	dom_string_unref(nsl_dom_str_node_removed);
	dom_string_unref(nsl_dom_str_subtree_modified);
	dom_string_unref(nsl_dom_str_attr_modified);
	dom_string_unref(nsl_dom_str_characterdata_modified);

	return NSL_OK;
}

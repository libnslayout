
LibNSLayout: The NetSurf browser layout engine
==============================================

This is being written to replace NetSurf's old built-in render code, which
had a number of issues.  Most notably it was unable to handle dynamic changes
to the document.

Dependencies
------------

LibNSLayout requires LibDOM and LibCSS from the NetSurf browser project.


Building
--------

Follow NetSurf's QUICK-START document to get and build the project's other
libraries.  With your `env/sh` sourced run the following commands:

    $ make
    $ make test
    $ make install

Or to test in debug mode:

    $ make VARIANT=debug test

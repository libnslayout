/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 * Copyright 2015 John-Mark Bell <jmb@netsurf-browser.org>
 */

/** \file include/libnslayout/nslayout.h
 * Layout object handling
 */

#ifndef nsl_nslayout_h_
#define nsl_nslayout_h_

#ifdef __cplusplus
extern "C"
{
#endif

#include <libcss/libcss.h>
#include <dom/dom.h>

#include <libnslayout/error.h>

/** A client-defined replaced element structure */
typedef struct nsl_replaced nsl_replaced;

/** A rectangle */
typedef struct nsl_rect {
	int x; /**< X position of left of rect in px */
	int y; /**< Y position of top  of rect in px */
	int w; /**< Width  of rect in px */
	int h; /**< Height of rect in px */
} nsl_rect;

/***/
enum nsl_dom_node_event_type {
	NSL_DOM_NODE_INSERTED,
	NSL_DOM_NODE_MODIFIED,
	NSL_DOM_NODE_REMOVED,
	NSL_DOM_NODE__COUNT,
};

/** Render list */
typedef struct nsl_render_list {
} nsl_render_list;

/** Opaque layout tree object */
typedef struct nsl_layout nsl_layout;


/**
 * A LibNSLayout request
 * Client calls to set replaced element intrinsic dimensions.
 *
 * Passed to the client via nsl_callback
 */
typedef struct nsl_request {
	/** Request type */
	enum {
		NSL_GET_RESOURCE,
		NSL_RENDER,
		NSL_SET_EXTENTS,
		NSL_GET_INTRINSIC_SIZE
	} type;
	/** Request's type-specific parameters */
	union {
		struct {
			const char *url; /**< Absolute URL */
		} get_resource;
		struct {
			nsl_render_list *list; /**< Render list */
		} render;
		struct {
			unsigned int width;  /**< Document width  in px */
			unsigned int height; /**< Document height in px */
		} set_extents;
		struct {
			nsl_replaced *replaced; /** A replacement object */
		} get_intrinsic_size;
	} request;
	/** Request's type-specific return values */
	union {
		struct {
			nsl_replaced **replaced; /** Replacement object */
		} get_resource;
		struct {
			unsigned int *width;  /** Replacement object's width  */
			unsigned int *height; /** Replacement object's height */
		} get_intrinsic_size;
	} response;
} nsl_request;


/**
 * Client calls for DOM tree change events.
 *
 * \param[in] layout  The layout requiring update for DOM change.
 * \param[in] node    The DOM node which is concerned in the event.
 * \param[in] type    The type of DOM change event.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_dom_node_event(
		nsl_layout *layout,
		dom_event_target *node,
		enum nsl_dom_node_event_type type);

/**
 * Client calls to set node as client-replaced.
 *
 * \param[in] layout    The layout to replace an element in.
 * \param[in] node      The DOM node which is to be replaced.
 * \param[in] replaced  The client's replacement object to register with node.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_node_event_set_replaced(
		nsl_layout *layout,
		dom_event_target *node,
		nsl_replaced *replaced);


/**
 * Client calls to set replaced element intrinsic dimensions.
 *
 * \param[in] layout  The layout to replace an element in.
 * \param[in] node    The DOM node which is to be replaced.
 * \param[in] width   Width in pixels.
 * \param[in] height  Height in pixels.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_node_event_set_intrinsic_dimensions(
		nsl_layout *layout,
		dom_event_target *node,
		unsigned int width,
		unsigned int height);


/**
 * Client calls to tell NSLayout that everything requires reselection.
 *
 * \param[in] layout  The layout to who's selection context has changed.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_selection_context_updated(
		nsl_layout *layout);


/**
 * Initialise LibNSLayout
 *
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_init(void);

/**
 * Finalise LibNSLayout
 *
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_fini(void);

/**
 * LibNSLayout client callback function
 *
 * \param[in]     layout  The layout we're making a request for.
 * \param[in]     pw      The client's private data for this layout.
 * \param[in,out] req     The request details.
 * \return NSL_OK on success, appropriate error otherwise.
 */
typedef nsl_error (*nsl_callback)(
		nsl_layout *layout,
		void *pw,
		nsl_request *req);

/**
 * Create a Layout object for a given DOM
 *
 * \param[in]  doc      The LibDOM document to build a layout for.
 * \param[in]  css_ctx  The LibCSS selection context for the document.
 * \param[in]  media    The LibCSS media to use when selecting.
 * \param[in]  cb       The client's private data for this layout.
 * \param[in]  pw       The client's private data for this layout.
 * \param[out] layout   Returns a pointer to the created layout object.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_layout_create(
		dom_document *doc,
		css_select_ctx *css_ctx,
		css_media_type *media,
		nsl_callback cb,
		void *pw,
		nsl_layout **layout);

/**
 * Destroy a Layout object
 *
 * \param[in] layout  Returns a pointer to the created layout object.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_layout_destroy(
		nsl_layout *layout);

/**
 * Update the viewport for a layout
 *
 * Note: Before this is called, the layout engine will create internal
 *       data structures for the document, but will not start to position
 *       things and will not emit render lists.
 *
 * \param[in] layout    Layout to set viewport for.
 * \param[in] viewport  Viewport dimensions and offset.
 * \param[in] scale     Rendering scale.
 * \param[in] dpi       DPI of render target with viewport.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_update_viewport(
		nsl_layout *layout,
		nsl_rect *viewport,
		css_fixed scale,
		unsigned int dpi);

/**
 * Find the area occupied by element.
 *
 * \param[in]  layout   Layout to locate an element in.
 * \param[in]  element  Element to get area of.
 * \param[out] area     Returns area with position relative to viewport.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_element_get_location(
		nsl_layout *layout,
		dom_element *element,
		nsl_rect *area);

/**
 * Find the top-most element at a given point, in terms of z-order.
 *
 * \param[in]  layout   Layout to find an element in.
 * \param[in]  x        Mouse x-coordinate (viewport relative).
 * \param[in]  y        Mouse y-coordinate (viewport relative).
 * \param[out] element  Returns the element we found.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_element_at_point(
		nsl_layout *layout,
		unsigned int x,
		unsigned int y,
		dom_event_target **element);

/**
 * Mark an element (or part of it) as needing redraw.
 *
 * \param[in] layout    Layout to indicate redraw is required for.
 * \param[in] element   Element to mark as needing redraw.
 * \param[in] rel_area  Area of element to redraw relative to object's top-left.
 *                      May be NULL, to redraw whole element.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_layout_dirty_element(
		nsl_layout *layout,
		dom_element *element,
		nsl_rect *rel_area);

/**
 * Mark an area as needing redraw.
 *
 * \param[in] layout  Layout to indicate redraw is required for.
 * \param[in] area    Area to redraw relative to viewport's top-left.
 * \return NSL_OK on success, appropriate error otherwise.
 */
nsl_error nsl_layout_dirty_area(
		nsl_layout *layout,
		nsl_rect *area);

#ifdef __cplusplus
}
#endif

#endif

/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file include/libnslayout/error.h
 * Layout object handling
 */

#ifndef nsl_error_h_
#define nsl_error_h_

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * Number of bits in an `nsl_error` that indicate the source of the error.
 */
#define NSL_ERROR_PROV 8

/**
 * Libnslayout return codes
 *
 * NSL_OK indicates no error.
 */
typedef enum nsl_error {
	/** No error code */
	NSL_OK            = 0,

	/** Error provenance (bits 0..7) */
	NSL_NSLAYOUT      = (1 << 0),
	NSL_LIBDOM        = (1 << 1),
	NSL_LIBCSS        = (1 << 2),

	/** LibNSLayout errors (bits 8..31) */
	NSL_NO_MEM        = (1 << NSL_ERROR_PROV) | NSL_NSLAYOUT,
} nsl_error;


/**
 * Get error provenance.
 *
 * \param[in]  err  Error code to test
 * \return error provenance
 */
static inline nsl_error nsl_error_provenance(nsl_error err)
{
	return err & ((1 << NSL_ERROR_PROV) - 1);
}


/**
 * Check if error is from libnslayout
 *
 * \param[in]  err  Error code to test
 * \return true iff error is from libnslayout
 */
static inline bool nsl_error_is_layout(nsl_error err)
{
	return err & NSL_NSLAYOUT;
}


/**
 * Check if error is from libdom
 *
 * \param[in]  err  Error code to test
 * \return true iff error is from libdom
 */
static inline bool nsl_error_is_libdom(nsl_error err)
{
	return err & NSL_LIBDOM;
}

/**
 * Check if error is from libcss
 *
 * \param[in]  err  Error code to test
 * \return true iff error is from libcss
 */
static inline bool nsl_error_is_libcss(nsl_error err)
{
	return err & NSL_LIBCSS;
}

/**
 * Turn libnslayout return code into libnslayout error
 *
 * \param[in]  err  Error code to convert
 * \return libnslayout error
 */
static inline nsl_error nsl_error_to_layout(nsl_error err)
{
	return err;
}

/**
 * Turn libnslayout return code into libdom error
 *
 * \param[in]  err  Error code to convert
 * \return dom exception
 */
static inline dom_exception nsl_error_to_libdom(nsl_error err)
{
	return err >> 8;
}

/**
 * Turn libnslayout return code into libcss error
 *
 * \param[in]  err  Error code to convert
 * \return libcss error
 */
static inline css_error nsl_error_to_libcss(nsl_error err)
{
	return err >> 8;
}

#ifdef __cplusplus
}
#endif

#endif

/*
 * This file is part of LibNSLayout's tests
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

#ifndef nsl_tests_h_
#define nsl_tests_h_

#include <signal.h>

#include <check.h>

#include <libnslayout/nslayout.h>

extern void nsl_assert_suite(SRunner *);
extern void nsl_basic_suite(SRunner *);

#endif

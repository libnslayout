/*
 * This file is part of LibNSLayout's tests
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

#include <check.h>
#include <stdlib.h>
#include <string.h>

#include "tests.h"

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

static nsl_error nsl_test_callback(
		nsl_layout *layout,
		void *pw,
		nsl_request *req)
{
	UNUSED(req);
	UNUSED(layout);
	UNUSED(pw);
	return NSL_OK;
}

START_TEST (test_nsl_layout_create_ok)
{
	nsl_layout *layout = NULL;
	nsl_error error;
	dom_exception dom_error;
	css_error css_err;
	dom_document *doc;
	css_select_ctx *css_ctx;
	css_media_type media = CSS_MEDIA_SCREEN;

	/* Create a DOM document */
	dom_error = dom_implementation_create_document(DOM_IMPLEMENTATION_HTML,
			NULL, NULL, NULL, NULL, NULL, &doc);
	ck_assert(dom_error == DOM_NO_ERR);

	/* Create a selection context (with no sheets added) */
	css_err = css_select_ctx_create(&css_ctx);
	ck_assert(css_err == CSS_OK);

	ck_assert(nsl_init() == NSL_OK);

	error = nsl_layout_create(doc,
			css_ctx,
			&media,
			nsl_test_callback,
			NULL,
			&layout);
	fail_unless(error == NSL_OK,
			"Unable to create layout");
	fail_unless(layout != NULL,
			"Returned OK but str was still NULL");

	error = nsl_layout_destroy(layout);
	fail_unless(error == NSL_OK,
			"Unable to destroy layout");

	ck_assert(nsl_fini() == NSL_OK);

	css_err = css_select_ctx_destroy(css_ctx);
	ck_assert(css_err == CSS_OK);

	dom_node_unref(doc);
}
END_TEST


void nsl_basic_suite(SRunner *sr)
{
	Suite *s = suite_create("libnslayout: nslayout object tests");
	TCase *tc_layout_basic = tcase_create("Creation/Destruction");

	tcase_add_test(tc_layout_basic, test_nsl_layout_create_ok);
	suite_add_tcase(s, tc_layout_basic);

	srunner_add_suite(sr, s);
}

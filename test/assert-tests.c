/*
 * This file is part of LibNSLayout's tests
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

#include <check.h>
#include <stdlib.h>
#include <string.h>

#include "tests.h"

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif

/* TODO: Test for each individual param being NULL. */
START_TEST (test_nsl_layout_create_aborts1)
{
	nsl_layout *layout;

	(void) nsl_layout_create(NULL, NULL, NULL, NULL, NULL, &layout);
}
END_TEST

/* TODO: Test for each individual param being NULL. */
START_TEST (test_nsl_layout_destroy_aborts1)
{
	(void) nsl_layout_destroy(NULL);
}
END_TEST


void nsl_assert_suite(SRunner *sr)
{
	Suite *s = suite_create("libnslayout: API Assert tests");
	TCase *tc_assert = tcase_create("Creation/Destruction");

	tcase_add_test_raise_signal(
			tc_assert,
			test_nsl_layout_create_aborts1,
			SIGABRT);
	tcase_add_test_raise_signal(
			tc_assert,
			test_nsl_layout_destroy_aborts1,
			SIGABRT);
	suite_add_tcase(s, tc_assert);

	srunner_add_suite(sr, s);
}

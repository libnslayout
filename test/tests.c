/*
 * This file is part of LibNSLayout's tests
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

#include <assert.h>
#include <stdlib.h>

#include <check.h>

#include "tests.h"

#ifndef UNUSED
#define UNUSED(x) ((x) = (x))
#endif

#ifndef NDEBUG
/* This means that assertion failures are silent in tests */
void __assert_fail(const char *__assertion, const char *__file,
		unsigned int __line, const char *__function) {
	(void)__assertion;
	(void)__file;
	(void)__line;
	(void)__function;
	abort();
}
#endif

int main(int argc, char **argv)
{
	int number_failed = 0;
	SRunner *sr;

	UNUSED(argc);
	UNUSED(argv);

	sr = srunner_create(suite_create("Test suite for libnslayout"));

#ifndef NDEBUG
	nsl_assert_suite(sr);
#endif
	nsl_basic_suite(sr);

	srunner_set_fork_status(sr, CK_FORK);
	srunner_run_all(sr, CK_ENV);
	number_failed = srunner_ntests_failed(sr);

	srunner_free(sr);

	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015-2017 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/dom/debug.h
 * DOM debug
 */

#ifndef nsl_dom_debug_h_
#define nsl_dom_debug_h_

/** Define to enable DOM trace debug output */
#undef NSL_DOM_TRACE
#define NSL_DOM_TRACE

#include "util/util.h"

struct dom_event;

/** Don't use directly */
void nsl__dom_debug_dump_event(const struct dom_event *evt);


/**
 * Dump debug for dom event
 *
 * \param[in] evt  Dump debug concerning a DOM event.
 */
static inline void nsl_dom_debug_dump_event(const struct dom_event *evt)
{
#ifdef NSL_DOM_TRACE
	nsl__dom_debug_dump_event(evt);
#else
	UNUSED(evt);
#endif
}


#endif

/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/dom/watcher.h
 * Interface to DOM mutation watching.
 */

#ifndef nsl_dom_watcher_h_
#define nsl_dom_watcher_h_

struct dom_document;
struct nsl_dom_watcher;

/**
 * DOM watcher's mutation types
 */
enum nsl_dom_watcher_type {
	NSL_DOM_WATCHER_NODE_INSERTED,
	NSL_DOM_WATCHER_NODE_REMOVED,
	NSL_DOM_WATCHER_SUBTREE_MODIFIED,
	NSL_DOM_WATCHER_ATTR_MODIFIED,
	NSL_DOM_WATCHER_CHAR_DATA_MODIFIED,
	NSL_DOM_WATCHER__COUNT,
};


/**
 * Callback function for dom modifications.
 *
 * \param[in]  type       The mutation type.
 * \param[in]  node       The target node.  (Caller yields ownership.)
 * \param[in]  node_type  The type of node.
 * \param[in]  pw         The dom watcher owner's private data.
 * \return NSL_OK on success, appropriate error otherwise.
 */
typedef bool (*nsl_dom_watcher_cb)(
		enum nsl_dom_watcher_type type,
		dom_event_target *node,
		dom_node_type node_type,
		void *pw);


/**
 * Create DOM change watcher for a DOM document.
 *
 * \param[out]  watcher_out  Returns a dom watcher object for layout.
 * \param[in]   document     DOM document to create watcher for.
 * \param[in]   watcher_cb   Function to call when dom modification happens.
 * \param[in]   pw           Private data passed back to `watcher_cb`.
 * \return NSL_OK on success, appropriate error otherwise.
 */
bool nsl_dom_watcher_create(
		struct nsl_dom_watcher **watcher_out,
		dom_document *document,
		nsl_dom_watcher_cb watcher_cb,
		void *pw);


/**
 * Destroy a document change DOM change watcher.
 *
 * \param[in]  watcher  DOM change watcher to destroy.
 * \return NSL_OK on success, appropriate error otherwise.
 */
bool nsl_dom_watcher_destroy(
		struct nsl_dom_watcher *watcher);

#endif

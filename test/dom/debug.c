/*
 * This file is part of LibNSLayout
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015-2017 Michael Drake <tlsa@netsurf-browser.org>
 */

/** \file src/dom/debug.c
 * DOM debug
 */

#include <stdio.h>

#include <dom/dom.h>

#include "debug.h"


#ifdef NSL_DOM_TRACE

/**
 * Convert a dom node type to a string
 *
 * \param[in]  type  DOM node type
 * \return appropriate string.
 */
static inline const char *nsl__dom_node_type_to_string(dom_node_type type)
{
	static const char *str[DOM_NODE_TYPE_COUNT] = {
		[DOM_ELEMENT_NODE]                = "ELEMENT_NODE",
		[DOM_ATTRIBUTE_NODE]              = "ATTRIBUTE_NODE",
		[DOM_TEXT_NODE]                   = "TEXT_NODE",
		[DOM_CDATA_SECTION_NODE]          = "CDATA_SECTION_NODE",
		[DOM_ENTITY_REFERENCE_NODE]       = "ENTITY_REFERENCE_NODE",
		[DOM_ENTITY_NODE]                 = "ENTITY_NODE",
		[DOM_PROCESSING_INSTRUCTION_NODE] = "PROCESSING_INSTRUCTION_NODE",
		[DOM_COMMENT_NODE]                = "COMMENT_NODE",
		[DOM_DOCUMENT_NODE]               = "DOCUMENT_NODE",
		[DOM_DOCUMENT_TYPE_NODE]          = "DOCUMENT_TYPE_NODE",
		[DOM_DOCUMENT_FRAGMENT_NODE]      = "DOCUMENT_FRAGMENT_NODE",
		[DOM_NOTATION_NODE]               = "NOTATION_NODE"
	};

	return str[type];
}


/* Exported function, documented in src/dom/debug.h */
void nsl__dom_debug_dump_event(const struct dom_event *evt)
{
	dom_event_target *node = NULL;
	dom_node_type node_type;
	dom_string *name = NULL;
	dom_string *type = NULL;
	dom_exception exc;

	printf("  DOM Event: ");

	/* Ugly test to see what events come out */
	exc = dom_event_get_target(evt, &node);
	if ((exc != DOM_NO_ERR) || (node == NULL)) {
		printf("FAILED to get target node!\n");
		goto fail;
	}

	exc = dom_node_get_node_type(node, &node_type);
	if (exc != DOM_NO_ERR) {
		printf("FAILED to get target node type!\n");
		goto fail;
	}

	if (node_type == DOM_ELEMENT_NODE) {
		exc = dom_node_get_node_name(node, &name);
		if ((exc != DOM_NO_ERR) || (name == NULL)) {
			printf("FAILED to get target node name!\n");
			goto fail;
		}
	}

	exc = dom_event_get_type(evt, &type);
	if ((exc != DOM_NO_ERR) || (type == NULL)) {
		printf("FAILED to get event type!\n");
		goto fail;
	}

	if (node_type == DOM_ELEMENT_NODE) {
		printf("<%s> %s",
				dom_string_data(name),
				dom_string_data(type));
	} else {
		printf("%s %s",
				nsl__dom_node_type_to_string(node_type),
				dom_string_data(type));
	}

fail:
	if (type != NULL) dom_string_unref(type);
	if (name != NULL) dom_string_unref(name);
	if (node != NULL) dom_node_unref(node);

	printf("\n");
}

#endif

/*
 * This file is part of LibNSLayout's tests
 * Licensed under the ISC License, http://opensource.org/licenses/ISC
 * Copyright 2015 Michael Drake <tlsa@netsurf-browser.org>
 */

#include "../test/test-loader.c"

/*
 * cd ../ && make && make install && cd dev/ && gcc -Wall -Wextra -Werror `pkg-config libnslayout --cflags` main.c `pkg-config libnslayout --libs` && ./a.out ; cd ~/dev-netsurf/workspace/libnslayout/dev
 */


static void nsl_test_lwc_iterator(lwc_string *str, void *pw)
{
	UNUSED(pw);

	printf("  [%3u] %.*s\n", str->refcnt,
			(int)lwc_string_length(str),
			lwc_string_data(str));
}

int main(void)
{
	struct test_loader_buffer *buffer;
	bool ok;

	ok = test_loader_load_path_to_buffer(
			"test-writing-mode.html",
			&buffer);
	if (!ok) return EXIT_FAILURE;

	nsl_init();

	ok = test_loader(buffer, CSS_MEDIA_ALL, 15);
	if (!ok) return EXIT_FAILURE;

	nsl_fini();

	test_loader_free_buffer(buffer);

	printf("Remaining lwc strings:\n");
	lwc_iterate_strings(nsl_test_lwc_iterator, NULL);

	return EXIT_SUCCESS;
}
